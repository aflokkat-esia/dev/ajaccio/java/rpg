package personnages;

import objects.Objet;

public class Ennemi {
    private int vie;
    final private int attaqueMax;
    final private int exp;
    private boolean vivant = true;
    private Objet loot = new Objet("Armure", 20, 20);

    public Ennemi(int vie, int attaqueMax, int exp) {
        this.vie = vie;
        this.attaqueMax = attaqueMax;
        this.exp = exp;
    }

    public boolean attaquer(Heros heros) {
        if (heros.estVivant()) {
            heros.prendreDesDegats(this);
            return !heros.estVivant();
        }
        return true;
    }

    public void prendreDesDegats(int degats, Heros heros) {
        if (this.vie - degats <= 0) {
            this.vie = 0;
            this.mourir(heros);
        } else {
            this.vie -= degats;
            System.out.println("Vie de l'ennemi : " + this.vie);
        }
    }

    private void mourir(Heros heros) {
        System.out.println("Ennemi mort ! Vous récupérez son expérience.");
        this.vivant = false;
        heros.gagnerExp(this.exp);
        heros.ajouterObjet(this.loot);
    }

    public boolean estVivant() {
        return this.vivant;
    }

    public int getAttaqueMax() {
        return this.attaqueMax;
    }

    public int getVie() {
        return vie;
    }
}
