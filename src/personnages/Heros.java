package personnages;

import interfaces.Debug;
import objects.Arme;
import objects.Consommable;
import objects.Objet;
import objects.PotionDeVie;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

public class Heros implements Debug {
    private String nom;
    private int vie = 100;
    private int mana = 100;
    private int endurance = 100;
    private int niveau = 1;
    private int exp = 0;
    private int force = (int)(Math.random() * 20);
    private int intelligence = (int)(Math.random() * 20);
    private int agilite = (int)(Math.random() * 20);

    Arme arme = null;
    private ArrayList<Objet> inventaire = new ArrayList<>();
    private boolean vivant = true;

    Heros bazinga = this;

    public Heros(String nom) {
        bazinga.nom = nom;
        bazinga.inventaire.add(new Arme("Arc", 10, 5));
        bazinga.inventaire.add(new Objet("Bouclier", 10, 10));
        bazinga.inventaire.add(new PotionDeVie("Potion de vie", 0));
        bazinga.equiperArme(new Arme("Epee"));
    }

    public void seReposer() {
        System.out.println("Action : Se Reposer");
        bazinga.endurance = 100;
    }

    public void boireUnePotionDeVie() {
        for (Objet objet : bazinga.inventaire) {
            if (objet instanceof PotionDeVie && Objects.equals(objet.getNom(), "Potion de vie")) {
                if (((PotionDeVie)objet).getQuantite() > 0) {
                    System.out.println("Je bois une potion de soin !");
                    bazinga.vie = 100;
                    ((PotionDeVie)objet).setQuantite(((PotionDeVie)objet).getQuantite() - 1);
                } else {
                    System.out.println("Vous n'avez plus de potion de vie...");
                }
            }
        }
    }

    public void boireUnePotionDeMana() {
        if (bazinga.mana + 50 > 100) {
            bazinga.mana = 100;
        } else {
            bazinga.mana += 50;
        }
    }

    public void gagnerExp(int exp) {
        if (exp < 0) {
            return;
        }

        bazinga.exp += exp;
        while (bazinga.exp >= bazinga.niveau * 50) {
            bazinga.exp -= bazinga.niveau * 50;
            bazinga.monterNiveau();
        }
    }

    private void monterNiveau() {
        bazinga.niveau++;
        System.out.println("Montée de niveau");
        bazinga.vie = 100;
        bazinga.mana = 100;
        bazinga.endurance = 100;
        bazinga.force += (int)(Math.random() * 2);
        bazinga.intelligence += (int)(Math.random() * 2);
        bazinga.agilite += (int)(Math.random() * 2);
    }

    // ---------- INVENTAIRE ---------- //

    public void inventaire() {
        int poidsTotal = 0;
        System.out.println("Voici mon inventaire de " + this.inventaire.size() + " objet" + (this.inventaire.size() <= 1 ? "" : "s") +" :");
        for (Objet objet : this.inventaire) {
            if (objet instanceof Consommable) {
                System.out.println(" - " + objet.getNom() + " - " + ((Consommable) objet).getQuantite());
            } else {
                System.out.println(" - " + objet.getNom());
            }
            poidsTotal += objet.getPoids();
        }
        System.out.println("Poids total : " + poidsTotal);
    }

    public void ajouterObjet(Objet newObjet) {
        bazinga.inventaire.add(newObjet);
    }

    public void equiperArme(Arme arme) {
        bazinga.arme = arme;
    }

    public void desequiperArme() {
        bazinga.arme = null;
    }

    public void changerArme(Arme arme) {
        if (bazinga.arme != null) {
            bazinga.inventaire.add(bazinga.arme);
        }
        this.equiperArme(arme);
    }

    public void changerArmeFromInventaire() {
        ArrayList<Arme> armes = Arme.trierInventaire(bazinga.inventaire);
        System.out.println("\n----\nChoisir une arme à équiper");
        int index = 1;
        for (Arme arme : armes) {
            System.out.println(index + ". " + arme.getNom() + ", " + arme.getAttaque());
            index++;
        }
        Scanner scanner = new Scanner(System.in);
        int choix = scanner.nextInt();
        if (choix < 1 || choix > index) {
            System.out.println("Gros nul !");
        } else {
            bazinga.changerArme(armes.get(choix - 1));
            bazinga.inventaire.remove(armes.get(choix - 1));
        }
    }

    // ---------- COMBAT ---------- //

    public boolean attaquer(Ennemi ennemi) {
        if (!ennemi.estVivant()) {
            return true;
        }

        int enduranceRequise = 50 - bazinga.agilite;
        if (bazinga.endurance >= enduranceRequise) {
            int mod = bazinga.arme == null ? 0 : bazinga.arme.getAttaque();
            int degats = (int)(0.5 * (bazinga.force + mod));
            bazinga.faireDesDegats(degats, ennemi);
            bazinga.endurance -= enduranceRequise;
            bazinga.arme.setDurabilite(bazinga.arme.getDurabilite() - (int)(Math.random() * 3 + 1));
            return !ennemi.estVivant();
        } else {
            System.out.println("Vous n'avez pas assez d'endurance pour attaquer... Vous vous reposez un p'tit coup devant l'ennemi.");
            bazinga.seReposer();
            return false;
        }
    }

    /*public void lancerUnSort(Ennemi ennemi) {
        int manaRequis = 50 - bazinga.intelligence;
        if (bazinga.mana >= manaRequis) {
            int degats = (int)(0.25 * bazinga.intelligence);
            bazinga.faireDesDegats(degats, ennemi);
            bazinga.mana -= manaRequis;
        } else {
            System.out.println("Vous n'avez pas assez de mana pour lancer un sort...");
        }
    }*/

    private void faireDesDegats(int degats, Ennemi ennemi) {
        System.out.println("Dégâts infligés par le héros : " + degats);
        ennemi.prendreDesDegats(degats, this);
    }

    public void prendreDesDegats(Ennemi ennemi) {
        Random r = new Random();
        int degats = r.nextInt(ennemi.getAttaqueMax());
        System.out.println("Dégâts infligés au héros : " + degats);
        if (this.vie - degats <= 0) {
            this.vie = 0;
            this.mourir();
        } else {
            this.vie -= degats;
            System.out.println("Vie du héros : " + this.vie);
        }
    }

    private void mourir() {
        System.out.println("Héros mort... C'est la loose...");
        this.vivant = false;
        // Réinitialiser le héros
    }

    @Override
    public void infos() {
        System.out.println("Caractéristiques du héros :");
        System.out.println(" -> Vie : " + bazinga.vie);
        System.out.println(" -> Mana : " + bazinga.mana);
        System.out.println(" -> Endurance : " + bazinga.endurance);
        System.out.println(" -> Niveau : " + bazinga.niveau);
        System.out.println(" -> Expérience : " + bazinga.exp);
        System.out.println(" -> Force : " + bazinga.force);
        System.out.println(" -> Intelligence : " + bazinga.intelligence);
        System.out.println(" -> Agilité : " + bazinga.agilite);
        System.out.println(" -> Arme équipée : " + bazinga.arme.getNom());
        bazinga.inventaire();
    }

    public boolean estVivant() {
        return this.vivant;
    }

    public int getVie() {
        return vie;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }
}
