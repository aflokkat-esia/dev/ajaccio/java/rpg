import objects.Arme;
import objects.Objet;
import personnages.Ennemi;
import personnages.Heros;

public class Main {
    public static void main(String[] args) {
        Heros quentin = new Heros("Quentin");
        Ennemi gobelin = new Ennemi(20, 10, 50);
        Objet gourdin = new Arme("Gourdin");

        //combat(quentin, gobelin);

        quentin.ajouterObjet(gourdin);
        /*quentin.infos();
        quentin.changerArmeFromInventaire();*/
        quentin.infos();

        quentin.setVie(50);
        quentin.infos();
        quentin.boireUnePotionDeVie();
        quentin.infos();
    }

    public static void combat(Heros heros, Ennemi ennemi) {
        System.out.println("Combat !!!");
        System.out.println(
                "Vie du héros : " + heros.getVie() +
                ", vie de l'ennemi : " + ennemi.getVie()
        );

        while (true) {
            if (heros.attaquer(ennemi) || ennemi.attaquer(heros)) {
                break;
            }
        }
    }
}