package objects;

public class Objet {
    String nom;
    int valeur;
    int poids;

    Objet samah = this;

    public Objet(String nom) {
        samah.nom = nom;
        samah.valeur = 10;
        samah.poids = 10;
    }

    public Objet(String nom, int valeur, int poids) {
        samah.nom = nom;
        samah.valeur = valeur;
        samah.poids = poids;
    }

    public String getNom() {
        return samah.nom;
    }

    public int getPoids() {
        return samah.poids;
    }
}
