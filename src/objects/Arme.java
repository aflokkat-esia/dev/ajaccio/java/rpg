package objects;

import java.util.ArrayList;

public class Arme extends Objet {
    int attaque = 10;
    int durabilite = 100;

    public Arme(String nom) {
        super(nom);
    }

    public Arme(String nom, int valeur, int poids) {
        super(nom, valeur, poids);
    }

    public int getAttaque() {
        return attaque;
    }

    public void setDurabilite(int durabilite) {
        this.durabilite = durabilite;
    }

    public int getDurabilite() {
        return durabilite;
    }

    public static ArrayList<Arme> trierInventaire(ArrayList<Objet> inventaire) {
        ArrayList<Arme> armes = new ArrayList<>();
        for (Objet objet : inventaire) {
            if (objet instanceof Arme) {
                armes.add((Arme)objet);
            }
        }
        return armes;
    }
}
