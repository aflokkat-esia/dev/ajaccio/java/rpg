package objects;

public abstract class Consommable extends Objet {
    int quantite;

    public Consommable(String nom, int quantite) {
        super(nom);
        this.quantite = quantite;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
}
